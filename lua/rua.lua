local f = io.open("../test/1.luac","rb")

if not f then return end

local code = f:read("*a")

local out = {}
for i=1,#code do
    table.insert(out,string.format("%02X",code:byte(i)))
end

print(table.concat(out," "))
