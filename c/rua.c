#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    unsigned char * code;
    int len;

    FILE * f = fopen("../test/1.luac", "r");
    if(!f)
    {
        printf("file open fail.");
        return 1;
    }

    fseek(f,0L,SEEK_END);
    len=ftell(f);
    code = (unsigned char*)malloc(len);
    printf("file length %d bytes\r\n",len);

    int t = 0;
    while(t++<len)
        printf("%02X ",code[t-1]);

    free(code);
    printf("\r\ndone.");
    return 0;
}
