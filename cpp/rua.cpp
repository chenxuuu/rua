#include <iostream>
#include <fstream>

int main(int argc, char *argv[])
{
    std::ifstream file("../test/1.luac",std::ios::in|std::ios::binary|std::ios::ate);

    if(!file.is_open())
    {
        std::cout << "file open fail" << std::endl;
        return 1;
    }
    int size = file.tellg();
    std::cout << "file length: " << size << " bytes." << std::endl;
    file.seekg (0, std::ios::beg);
    unsigned char * buffer = new unsigned char [size];
    file.read ((char *)buffer, size);
    file.close();

    int t;
    while(t++<size)
        printf("%02X ",buffer[t-1]);

    delete[] buffer;

    std::cout << std::endl << "done" << std::endl;
    return 0;
}
